.. note: SVN references are outdated, keep CentOS7 docs until we stop supporting CentOS7
   (when Blender 3.3 is no longer supported).

###################
Building Linux Libs
###################

This page contains notes on maintaining Linux libraries, it is more a working set of notes and not official documentation.

Things to do on Fresh Rocky8
*****************************

* Run: ``nano /etc/sysconfig/network-scripts/ifcfg-enp0s3`` where ``enp0s3`` may change based on the system.
  Change line `ONBOOT=yes` (was no). Reboot.

* Follow steps from:
  https://wiki.blender.org/wiki/Building_Blender/Linux/CentOS7ReleaseEnvironment

Add public SSH key
===================

Needed to log into the guest from the host.

.. code-block:: bash

   mkdir ~/.ssh
   chmod 700 ~/.ssh
   touch ~/.ssh/authorized_keys
   chmod 600 ~/.ssh/authorized_keys
   vi ~/.ssh/authorized_keys
   # Paste in public key.

``~/.bash_profile``:

.. code-block:: bash

   # .bash_profile

   # Get the aliases and functions
   if [ -f ~/.bashrc ]; then
     . ~/.bashrc
   fi

   # User specific environment and startup programs

   PATH=$PATH:$HOME/.local/bin

   export PATH

   # Custom Things.

   # WARNING: when we have to support multiple different environments at once,
   # this is no longer a good idea as switching to end environment from another will cause problems.

   if [ -f /etc/rocky-release ]; then
     source scl_source enable gcc-toolset-11
   else
     source scl_source enable devtoolset-9
   fi

   # Handy single character path shortcuts.
   alias B="cd /src/blender"
   alias L="cd /src/lib/linux_x86_64"
   alias D="cd /src/build_linux/deps"


SSH Into the Guest OS
*********************

While not essential, in general I find it much more convenient to SSH into the system,
it allows copy/paste on-screen text, larger terminal display and doesn't lock the cursor
to the window as VirtualBox does.

GUI-Less VirtualBox (host)
==========================

Running GUI-less is handy, allows to quit and restart the graphical session without interrupting the VM.

The VM can be started from a terminal as follows:

.. code-block:: bash

   VBoxHeadless --startvm "Redhat8" & disown

Run TMUX (guest)
================

Not essential, just useful to be able to reconnect via ssh.

Attach or join TMUX session, useful in case the SSH session disconnects (if the terminal is closed for e.g.).

.. code-block:: bash

   sudo dnf install tmux

.. code-block:: bash

   TERM=xterm-256color tmux new -As RedHat8


Mount the guest VM's filesystem (from the host)
===============================================

This is handy because it allows using development environment and tools from the host system,
accessing files can be a bit slow, otherwise this is convenient.

.. code-block:: bash

   sshfs -p 3022 cbarton@127.0.0.1:/ /mnt/ext

Port forwarding needs to be set to forward 3022 on the host to 22 on the guest from virtualbox.

----

Notes from Sergey
*****************

- Sometimes needs clean build.
- We don't do reproducible builds, so manually delete libraries before copying new ones over.
- SVN Add can ignore ``.a`` so make sure those are added (by default these get ignored).
- Deps and linking issue: Some deps pick up system libraries.
  This can cause ``make deps`` to fail when making ``deps`` on the development environment.
- Building libraries for future releases.
- Don't start on new libs until  stable release has been released (and the current libs are tagged).
- ``build_files/build_environment/linux/linux-centos7-setup.sh``
  Script should be kept small, make everything part of "make deps".
- ``build_files/build_environment/cmake/versions.cmake`` (is source of truth for deps),
  if comments need to be added for ``deps`` (what they are used for, any general notes).
- document why deps are needed - nice to have.
- Multiple Python's versions are kept in SVN (why?, easy bisecting).


Patch Building DOES NOT use the latest libraries?
*************************************************

This can trip you up when testing if new functionality works with newly added/updated libs.
The patch builder has logic to use the same version of libs when the patch was submitted.

The Solution is to use the branch-build functionality, instead of building patches.

Building Libraries
******************

Link Time Optimization (LTO)
============================

Link time optimization causes static libraries not to link when developers build with different GCC versions.

For example:

.. code-block:: bash

   /python/lib/libpython3.10.a’ generated with LTO version 11.2 instead of the expected 12.0
   compilation terminated.
   lto-wrapper: fatal error: /usr/bin/c++ returned 1 exit status
   compilation terminated.

So while the builds will work (and pass tests) on the VM, this should not be used.

Building One Library at a time with Multiple Jobs
=================================================

Building a single library at a time makes it much easier to debug when something goes wrong
as the output isn't mixed with unrelated libraries.

Building with a single thread has the down-side that each library is build with one job,
leaving cores idle and taking a very long time. There is also the problem of some large libraries being
a bottleneck causing long build times when other small libraries have finished building.

CMake doesn't provide an obvious way to handle this.

CD'ing into each build directory and running ``make -j $(nproc)`` is an option but requires manual intervention.

The solution is to run make and pass MAKEFLAGS, so the top-level make is single threaded,
but sub-processes use multiple jobs.

.. code-block:: bash

   make MAKEFLAGS="-j$(nproc)"

The ninja build system (used by ``meson``) will already use multiple jobs, so it doesn't need special handling.

Testing Libraries
*****************

Changes to libraries should rebuild Blender and pass all tests.

Slow rebuilds from GPU Kernels
==============================

A ``make release`` build is very slow as it builds all GPU kernels,
these are not used or necessary for testing ``make full`` build is sufficient for running tests.


Rebuilding All Libraries
************************

Instead of removing the ``deps`` directory, remove everything except for the downloads.

.. code-block:: bash

   rm -rf $(find /src/build_linux/deps -mindepth 1 -maxdepth 1 -type d -not -name packages)


Discussion on GIT-LFS with Brecht
*********************************

ideasman42
   Hey, not urgent but it would be good if the steps for updating GIT-LFS were documented somewhere
   (developer handbook I suppose).

   I though the last update I pushed was OK but saw Brecht fixed some files that shouldn't have been committed as LFS.

   For some reason I'm getting strange errors still when trying to update GIT-LFS,
   so it would be good to have the exact commands you use to perform an update.

   Then I can check if this is an issue with my local setup - whatever the case may be.
   My setup for updating libs is now pretty smooth - mostly this GIT-LFS stuff is taking most of the time whenever
   I've had to do updates recently.

brecht
   @ideasman42 I really just do ``git add``, ``git rm``, ``git commit`` as usual, nothing LFS specific

ideasman42
   brecht ugh, everything I'm doing is git lfs specific, seems like that may be the issue then.

brecht
   My ``~/.gitconfig`` contains the hooks::

      [filter "lfs"]
        clean = git-lfs clean -- %f
        smudge = git-lfs smudge -- %f
        process = git-lfs filter-process
        required = true

ideasman42
   My config is the same.

   I was trying to do ``git lfs untrack/track``
   which caused it to require old files exist when attempting to commit new files.

   ..under the false assumption that ``git add`` would not use lfs.

   @brecht so do you remove the old directory then add the new one each time?

brecht
   @ideasman42 yes, for e.g. oidn I did ``rm -rf lib/macos_arm64/openimagedenoise``
   and let ``make install`` for the deps recreate it then just ``git add`` any new files if needed,
   and no need to even do ``git rm`` since that will be done automatically when committing.

ideasman42
   @brecht great, will try this next update... final thing, ``.gitattributes`` should not be modified right?
   (unless it's done intentionally).
   Most actions using ``git lfs track`` want to modify ``.gitattributes``

brecht
   right

ideasman42
   Phew, okay... think that's all info I need then, thanks!

brecht
   ``git lfs track`` configures the patterns that will use LFS, but the existing set of file extensions should be good.

ideasman42
   Online docs/tutorials for ``git lfs`` often reference ``track`` command which lead me down that path.

brecht
   also for completeness, I do ``git commit openimagedenoise``,
   then ``git checkout HEAD -- .`` and ``git clean -f -d`` to remove the changes to the other folders
   there might be more elegant commands, just the ones I'm used to.
